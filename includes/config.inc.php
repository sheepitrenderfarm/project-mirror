<?php
$config = array();
$config['storage']['projects'] = '/tmp/projects/';
$config['storage']['max'] = 0; // max size available for storage, in B. 0 -> unlimited

$config['log']['dir'] = '/tmp/logs/';
$config['log']['debug'] = true;
$config['log']['stdout'] = false;

$config['tmp_dir'] = '/tmp';

$config['access']['life_span'] = 3600;

$config['main-website']['remote_addr_authorized'] = array('169.197.182.76', '2602:fced::225:b5ff:fe88:5c');
$config['main-website']['url'] = 'https://www.sheepit-renderfarm.com';
