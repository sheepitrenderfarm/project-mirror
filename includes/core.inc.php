<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

mb_internal_encoding('UTF-8');
require_once(dirname(__FILE__).'/config.inc.php');
$local_config = dirname(__FILE__).'/config.local.inc.php';
if (file_exists($local_config)) {
	require_once($local_config);
}

session_start();
require_once(dirname(__FILE__).'/../lib/vendor/autoload.php');

function isFromMainServer() {
	global $config, $_SERVER;
	if (is_array($config['main-website']['remote_addr_authorized'])) {
		foreach($config['main-website']['remote_addr_authorized'] as $ip) {
			if ($_SERVER['REMOTE_ADDR'] == $ip) {
				return true;
			}
		}
	}
	else if (is_string($config['main-website']['remote_addr_authorized'])) {
		// old syntax
		return $_SERVER['REMOTE_ADDR'] == $config['main-website']['remote_addr_authorized'];
	}
	
	return false;
}
