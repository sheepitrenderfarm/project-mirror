<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

class Logger {
	public static function append($data_, $level_='info') {
		global $config;
		
		if ($level_ == 'debug' && (isset($config['log']['debug']) && $config['log']['debug'] == true) == false) {
			return;
		}
		
		$addr = @$_SERVER['REMOTE_ADDR'];
		while (strlen($addr) < strlen('255.255.255.255')) { // to have nice log
			$addr .= ' ';
		}
		
		@file_put_contents(Logger::file(), @date('M j H:i:s').' - '.$addr.' - '.strtoupper($level_).' - '.$data_."\r\n", FILE_APPEND);
		if (isset($config['log']['stdout']) && $config['log']['stdout'] === true) {
			echo '('.strtoupper($level_).') '.$data_."\n";
		}
	}
	
	public static function file() {
		global $config;
		
		return $config['log']['dir'].'/root.log';
	}
	
	public static function debug($data_) {
		Logger::append($data_, 'debug');
	}

	public static function info($data_) {
		Logger::append($data_, 'info ');
	}

	public static function warning($data_) {
		Logger::append($data_, 'warn ');
	}

	public static function error($data_) {
		Logger::append($data_, 'error');
	}

	public static function critical($data_) {
		Logger::append($data_, 'critical');
	}
}
