<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

class InstallCheckerAPCU implements InstallChecker {
	public function name() {
		return 'APCU configuration';
	}
	
	public function check() {
		$access = new Access();
		$access->setId(uniqid());
		$access->setPath('path_'.time());
		
		if ($access->add() == false) {
			return '<span style="color: red;">Failed to add Access to database</span>';
		}
		
		if (is_object(Access::load($access->getId()) == false)) {
			return '<span style="color: red;">Import of Access from database failed</span>';
		}
		
		if ($access->remove() == false) {
			return '<span style="color: red;">Failed to remove Access from database</span>';
		}
		
		if (is_object(Access::load($access->getId()) == true)) {
			return '<span style="color: red;">Failed to cleanup Access from database</span>';
		}
		
		return '<span style="color: green;">OK</span>';
	}
}
