<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

class InstallCheckerConfig implements InstallChecker {
	public function name() {
		return 'Configuration file';
	}
	
	public function check() {
		global $config;
		
		
		if (isset($config['storage']['projects'])) {
			if (substr($config['storage']['projects'], -1) != '/') {
				return $this->failed('$config[\'storage\'][\'projects\'] must end with a \'/\'');
			}
		}
		
		return $this->ok();
	}
	
	private function ok() {
		return '<span style="color: green;">OK</span>';
	}
	
	private function failed($msg) {
		return '<span style="color: red;">'.$msg.'</span>';
	}
}
