<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

require_once(dirname(__FILE__).'/includes/core.inc.php');

use Phyrexia\Http\Client as HttpClient;

function human_size($size) {
	$n = $size;
	if ($n < 0) {
		$n = -1 *$n;
	}
	
	if (1 <= ($n /1000000000000) && ($n /1000000000000) < 1000) {
		$str = sprintf("%.1f T", $n /1000000000000);
	}
	else if (1 < ($n /1000000000) && ($n /1000000000) < 1000) {
		$str = sprintf("%.1f G", $n /1000000000);
	}
	else if (1 < ($n /1000000) && ($n /1000000) < 1000) {
		$str = sprintf("%.1f M", $n /1000000);
	}
	else if (1 < ($n /1000) && ($n /1000) < 1000) {
		$str = sprintf("%.1f k", $n /1000);
	}
	else {
		$str = sprintf("%.1f", $n);
	}
	if ($size < 0) {
		$str = '-'.$str;
	}
	
	$str .= 'B';
	return $str;
}

echo '<html>';
echo '<body>';

$checkers = array();
$files = glob(dirname(__FILE__).'/classes/installcheckers/*.class.php');
if (is_array($files) == false) {
	echo "no install checker class directory";
	die();
}
foreach ($files as $path) {
	$pathinfo = pathinfo($path);
	if (array_key_exists('basename', $pathinfo) && array_key_exists('extension', $pathinfo) && $pathinfo['extension'] == 'php') {
		$checkers []= basename($pathinfo['basename'], '.class.php');
	}
}
?>
		<section class="slice">
				<div class="w-section inverse" style="padding-bottom:0;">
					<div class="container">
						<div class="row">
							<h1>Configuration</h1>
								<table class="table table-bordered table-striped table-comparision table-responsive sortable" border="1">
									<thead>
										<tr>
											<th>Setting</th>
											<th>Value</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="text-align: center; vertical-align: middle;">
												Version
											</td>
											<td style="text-align: center; vertical-align: middle;">
												<?php echo getenv('VERSION');?>
											</td>
										</tr>
										<tr>
											<td style="text-align: center; vertical-align: middle;">
												Max upload size
											</td>
											<td style="text-align: center; vertical-align: middle;">
												<?php echo ini_get('upload_max_filesize');?>
											</td>
										</tr>
										<tr>
											<td style="text-align: center; vertical-align: middle;">
												Remote allowed address
											</td>
											<td style="text-align: center; vertical-align: middle;">
												<?php
												if (is_array($config['main-website']['remote_addr_authorized'])) {
													print_r($config['main-website']['remote_addr_authorized']);
												}
												else {
													echo $config['main-website']['remote_addr_authorized'];
												}
												?>
											</td>
										</tr>
										<tr>
											<td style="text-align: center; vertical-align: middle;">
												Storage directory
											</td>
											<td style="text-align: center; vertical-align: middle;">
												<?php echo $config['storage']['projects'];?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<h1>Installation check</h1>
								<table class="table table-bordered table-striped table-comparision table-responsive sortable" border="1">
									<thead>
										<tr>
											<th>Name</th>
											<th>Result</th>
										</tr>
									</thead>
									<tbody>
									<?php
										foreach($checkers as $check_name) {
											$check = new $check_name();
											echo '<tr>';
											
											echo '<td style="text-align: center; vertical-align: middle;">';
											echo $check->name();
											echo '</td>';
											
											echo '<td style="text-align: center; vertical-align: middle;">';
											echo $check->check();
											echo '</td>';
											
											echo '</tr>';
										}
									?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<h1>Status</h1>
							<table class="table table-bordered table-striped table-comparision table-responsive sortable" border="1">
								<thead>
									<tr>
										<th>Setting</th>
										<th>Value</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="text-align: center; vertical-align: middle;">
											Hard drive usage
										</td>
										<td style="text-align: center; vertical-align: middle;">
										<?php
											$total = 0;
											foreach(glob($config['storage']['projects'].'/*.zip') as $f) {
												$total += filesize($f);
											}
											
											echo human_size($total);
											if ($config['storage']['max'] > 0) {
											    echo ' / ';
											    echo human_size($config['storage']['max']);
                                            }
										?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="row">
							<h1>Synced projects</h1>
							<table class="table table-bordered table-striped table-comparision table-responsive sortable" border="1">
								<thead>
									<tr>
										<th>Projects</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
                                <?php
                                $xml = (string)HttpClient::get($config['main-website']['url'].'/api.php?action=mirror_sync');
                                if (is_string($xml)) {
                                    $dom = new DomDocument('1.0', 'utf-8');
                                    $buf = @$dom->loadXML($xml);
                                    if ($dom->hasChildNodes()) {
                                        $file_nodes = $dom->getElementsByTagName('scene');
                                        if (is_null($file_nodes) == false) {
                                            foreach ($file_nodes as $a_node) {
                                                echo '<tr>';
                                                echo '<td style="text-align: center; vertical-align: middle;">';
                                                echo $a_node->getAttribute('id');
                                                echo '</td>';
                                                echo '<td style="text-align: center; vertical-align: middle;">';
                                                if (file_exists($config['storage']['projects'].((int)($a_node->getAttribute('id'))).'.zip')) {
                                                    echo '<span style="color:green">Synced</span>';
                                                }
                                                else {
                                                    echo '<span style="color:red">Not synced</span>';
                                                }
                                                echo '</td>';
                                                echo '</tr>';
                                            }
                                        }
                                    }
                                }
                                ?>
                                </tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
		</div>
	</body>
</html>
