CDN node of projects on https://www.sheepit-renderfarm.com

# Manual installation:
1. setup an https server
2. install php lib: php7.0-apcu php7.0-mbstring php7.0-xml php7.0-curl  (7.0 is not mandatory, it can be any 7.x)
3. install the required component by doing, "cd lib" them "composer install" (you can install php composer from the repositery or from https://getcomposer.org )
4. setup a daily cron to launch "php cleanup.php" as apache/www-data user, for example  ```su - www-data -s /bin/bash -c "php /home/www/sheepit-renderfarm.com/static/scene/cleanup.php"```
5. create directories where apache/www-data can write into: /var/local/storage/projects /var/local/storage/log
6. create a include/config.local.inc.php file with:
```php
<?php

$config['storage']['projects'] = '/var/local/storage/projects/';
// $config['storage']['max'] = 250000000000; // if you want to set a storage limit to 250GB uncomment this line
$config['log']['dir'] = '/var/local/storage/log';

?>
```
7. go on /status.php to check if the installation is ok


# Docker installation
See instruction on https://hub.docker.com/r/sheepitrenderfarm/project-mirror