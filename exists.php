<?php
/**
* Copyright (C) 2018 Laurent CLOUET
* Author Laurent CLOUET <laurent.clouet@nopnop.net>
**/

require_once(dirname(__FILE__).'/includes/core.inc.php');

if (isFromMainServer() == false) {
	Logger::error(__file__.':'.__line__.' request banned because remove_addr is \''.$_SERVER['REMOTE_ADDR'].'\'');
	die();
}

$path = basename($_REQUEST['path']); // to protect from path injection (../../something.zip)

$fullpath = $config['storage']['projects'].$path;
if (file_exists($fullpath)) {
	header('HTTP/1.1 200 OK');
}
else {
	Logger::debug(__file__.':'.__line__.' path does not exist '.$path);
	header('HTTP/1.1 404 Not Found');
}
