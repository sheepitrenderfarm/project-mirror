<?php
/**
* Copyright (C) 2017 Laurent CLOUET
* Author Laurent CLOUET <laurent.clouet@nopnop.net>
**/

require_once(dirname(__FILE__).'/includes/core.inc.php');

if (isFromMainServer() == false) {
	Logger::error(__file__.':'.__line__.' request banned because remove_addr is \''.$_SERVER['REMOTE_ADDR'].'\'');
	die();
}

$token = basename($_REQUEST['token']); // to protect from path injection (../../something.zip)
$path = basename($_REQUEST['path']);

$fullpath = $config['storage']['projects'].$path;
if (file_exists($fullpath)) {
	$access = new Access();
	$access->setId($token);
	$access->setPath($path);
	
	$access->add();
	
	$file_size = filesize($fullpath);
	header('Content-Length: '.$file_size);
}
else {
	Logger::error(__file__.':'.__line__.' path does not exist '.$path);
}
